/*var objects = {
    "0": {
        "class": "airplane_0",
        "x": 0, // metres
        "y": 0, // metres
        "orientation": 0, // radians
        "speed": 27 // meters per second
    }
};
*/

define([], function () {
    "use strict";

    var initialize = function (gameParams) {
        var gameState = 0;
        return gameState;
    };

    var update = function (gameState) {
        var newGameState = gameState + 1;
        return newGameState;
    };

    return {
        initialize: initialize,
        update: update

    };
});
