
define(["game/main", "renderer/main"], function (game, renderer) {
    "use strict";

    var initialize = function (startTime, window) {
        var gameState = game.initialize(startTime);
        var rendererState = renderer.initialize(gameState, window, startTime);

        return function (time) {
            gameState = game.update(gameState, time);
            rendererState = renderer.update(rendererState, gameState, time);
        };
    };
    var getMilliseconds = function() { return (new Date()).getTime(); };


    return function (window) {
        var update = initialize(getMilliseconds(), window);

        var update2 = function () {
            update(getMilliseconds());
        };
        update2();
        setInterval(update2, 1000);
    };

});
