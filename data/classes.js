define([], function () {
    "use strict";

    return {
        "classes": {
            "airplane_0": {
                "width": 15,
                "length": 7,
                "image": "airplane.png"
            }
        }
    };
});
